import requests
from bs4 import BeautifulSoup


def get_definition(word):
    """
    Return Word definition from aonaware Site
    """
    URL = 'http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(
        word)

    html = requests.get(URL).text
    soup = BeautifulSoup(html, "lxml")
    definition = soup.select("a > pre")
    # On ne cherche que dans un dictionnaire donc la liste de définition n'est composé que d'un éléménet
    elt = definition.pop()
    return elt.get_text()


def get_definition_payload(word):
    """
    Return Word definition from aonaware Site
    """
    payload = {'action': 'define',
               'dict': 'wn',
               'query': word}
    URL = 'http://services.aonaware.com/DictService/Default.aspx'
    html = requests.get(URL, params=payload).text
    soup = BeautifulSoup(html, "lxml")
    definition = soup.select("a > pre")
    # On ne cherche que dans un dictionnaire donc la liste de définition n'est composé que d'un éléménet
    elt = definition.pop()
    return elt.get_text()


def rechercher_fichier(nomFichier):
    """
    Recherche les définitions selon une liste de mots passés dans un fichier
    """
    lines = []
    with open(nomFichier) as f:
        lines = f.readlines()
    for line in lines:
        print(get_definition(line))


def rechercher_fichier_et_definir(fichierRecherche, fichierDefinition):
    lines = []
    with open(fichierRecherche) as f_nom:
        lines = f_nom.readlines()
    f_def = open(fichierDefinition, 'w+')
    for line in lines:
        f_def.write(get_definition(line))
        f_def.write('\n')


print("Obtention de la définition de breakfast avec la première méthode")
print(get_definition("Breakfast"))
print("Obtention de la définition de breakfast avec les données passés en payload")
print(get_definition_payload("Breakfast"))
print("Recherche de définitions selon les mots définis dans vocabulary.txt")
rechercher_fichier('vocabulary.txt')
print("Recherche de définitions selon les mots définis dans vocabulary.txt et écriture dans definitions.txt")
rechercher_fichier_et_definir('vocabulary.txt', 'definitions.txt')
