import requests
from bs4 import BeautifulSoup
import pandas as pd
UNI = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")
soup = BeautifulSoup(UNI.text, "lxml")
#questionList = (soup.find_all("a", {"class" : "question-hyperlink"}, limit=10))
questionList = soup.select("h3 > a.question-hyperlink", limit=10)
questionVote = soup.select("div.votes > span.vote-count-post > strong", limit=10)
questionAnswer = soup.select("div.status > strong", limit=10)
data = list()
for i in range(0,10):
    data.append((questionList[i].get_text(), questionVote[i].get_text(), questionAnswer[i].get_text()))
    print("Vote : " + questionVote[i].get_text() + " ; Réponses : " + questionAnswer[i].get_text() + " ; Question : " + questionList[i].get_text())

df = pd.DataFrame(data, columns=["titre" , "votes" , "answers"])
df.to_csv("questions.csv", sep=';', index=0)
#for elt in questionList:
#    print(elt.get_text())



