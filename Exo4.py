import requests
from bs4 import BeautifulSoup
# degree=DP
# orgUnit=orgunitContent://9ee7f4af-c6e8-406a-8292-dea5cdf178c1
# place=45000
payload = {'submit-form': '',
           'zone-item-id': 'zoneItem://c8e50408-29b9-4eb9-9b3f-1c209fb2d75b',
           'catalog': 'catalogue-2015-2016',
           'title': '',
           'textfield': '',
           'degree': 'DP',
           'orgUnit': 'orgunitContent://9ee7f4af-c6e8-406a-8292-dea5cdf178c1',
           'place': '45000'}
UNI = requests.post(
    'http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav', data=payload)
soup = BeautifulSoup(UNI.text, "lxml")
formationList = soup.select("li.hit > p > strong")
for elt in formationList:
    print(elt.get_text())
